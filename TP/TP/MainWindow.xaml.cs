﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace TP
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		int userTry = 0;
		int computerNumber;
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			Random random = new Random();
			computerNumber = random.Next(1, 51);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var test = input.Text;
			Regex regexNumber = new Regex("^[0-9]+$");

			if (regexNumber.IsMatch(test))
			{
				int number = int.Parse(test);
				 if (number ==  computerNumber)
				{
					userTry++;
					MessageBox.Show("BRAVO !! vous avez réussi après " + userTry + " Essai(s).\nMais malheureusement le voyage est trop chère pour vous,\nalors on vous offre un Larousse.", "Résultat");
				}
				else if (number > 0 && number <= computerNumber)
				{
					userTry++;
					MessageBox.Show("Dommage c'est plus haut", "Résultat");

				}
				else if (number >= computerNumber && number < 51)
				{
					userTry++;
					MessageBox.Show("Dommage c'est plus bas", "Résultat");
				}
				else
				{
					MessageBox.Show("Ce nombre n'est pas correcte", "Résultat");
				}
			}
			else
			{
				MessageBox.Show("il faut un nombre", "erreur");
			}
		}
	}
}