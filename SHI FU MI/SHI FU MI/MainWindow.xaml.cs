﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI_FU_MI
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		int? choice, ord;
		int winCount = 0, loseCount = 0, equalCount = 0, gameCount = 0;
		int winRate;
		Random rng = new Random();
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Stone_Click(object sender, RoutedEventArgs e)
		{
			choice = 0;
		}

		private void Paper_Click(object sender, RoutedEventArgs e)
		{
			choice = 1;
		}

		private void Cut_Click(object sender, RoutedEventArgs e)
		{
			choice = 2;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			ord = rng.Next(3);
			if(choice == null)
			{
				MessageBox.Show("Erreur veuilliez, Choisisez une action", "Erreur");
			}
			else
			{
				if (choice == 0 && ord == 2 || choice == 1 && ord == 0 || choice == 2 && ord == 1)
				{
					MessageBox.Show("Vous avez Gagné", "Victoire");
					winCount++;
					gameCount++;
				}
				else if (choice == 0 && ord == 1 || choice == 1 && ord == 2 || choice == 2 && ord == 0)
				{
					MessageBox.Show("vous avez Perdu", "Défaite");
					loseCount++;
					gameCount++;
				}
				else
				{
					MessageBox.Show("Egalité", "Egalité");
					equalCount++;
				}
				winRate = (int)Math.Round((float)(winCount*100) / gameCount);
				var show = "Victoire : " + winCount + " Défaites : " + loseCount + " Egalité : " + equalCount + " Avec : " + winRate + "% de winrate";
				screen.Text = show;
			}
		}
	}
}