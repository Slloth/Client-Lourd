﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace test.Customer
{
    /// <summary>
    /// Logique d'interaction pour addCustomer.xaml
    /// </summary>
    public partial class addCustomer : Page
    {
		private agenda db = new agenda();
		string regexName = @"^[A-Za-zéèàêâôûùïüç\-]+$";
		string regexSubject = @"^[A-Z0-9a-zéèàêâôûùïüç '\-]+$";
		string regexMail = @"[0-9a-zA-Z\.\-]+@[0-9a-zA-Z\.\-]+.[a-zA-Z]{2,4}";
		string regexPhone = @"^[0][0-9]{9}";
		string regexBudget = @"^[0-9]+$";

		public addCustomer()
        {
            InitializeComponent();
        }

		private void Seend_Click(object sender, RoutedEventArgs e)
		{
			//instanciation
			customers customers = new customers();
			bool isValid = true; //Permet de Vérifier les erreurs potentielles
			int error = 0; //Compte d'erreur(s)

			/*
			 Si le champs n'est pas vide, on entre dans la boucle
				 si le champ n'est pas conforme avec la REGEX on ecrit le message d'erreur dans le champ d'erreur, on change le booléen en FALSE et on ajoute 1 au compteur d'erreur
				 sinon on incremente le champ dans la colone concerné de la table customers
			Sinon on ecrit le message d'erreur dans le champ d'erreur, on change le booléen en FALSE et on ajoute 1 au compteur d'erreur
			*/
			//Insertion
			//LASTNAME
			if (!String.IsNullOrEmpty(lastName.Text))
			{
				if(!Regex.IsMatch(lastName.Text, regexName))
				{
					lastNameError.Text = "Ecrire un nom valide";
					isValid = false;
					error++;
				}
				else
				{
					customers.lastname = lastName.Text;
					lastNameError.Text = "";
				}
			}
			else
			{
				lastNameError.Text = "Ecrire un nom";
				isValid = false;
				error++;
			}
			//FIRSTNAME
			if (!String.IsNullOrEmpty(firstName.Text))
			{
				if(!Regex.IsMatch(firstName.Text, regexName))
				{
					firstNameError.Text = "Ecrire un prenom valide";
					isValid = false;
					error++;
				}
				else
				{
					customers.firstname = firstName.Text;
					firstNameError.Text = "";
				}
			}
			else
			{
				firstNameError.Text = "Ecrire un prenom";
				isValid = false;
				error++;
			}
			//MAIL
			if(!String.IsNullOrEmpty(mail.Text))
			{
				if(!Regex.IsMatch(mail.Text, regexMail))
				{
					mailError.Text = "Ecrire un mail valide";
					isValid = false;
					error++;
				}
				else
				{
					customers.mail = mail.Text;
					mailError.Text = "";
				}
			}
			else
			{
				mailError.Text = "Ecrire un Mail";
				isValid = false;
				error++;
			}
			//PHONENUMBER
			if (!String.IsNullOrEmpty(phoneNumber.Text))
			{
				if(!Regex.IsMatch(phoneNumber.Text, regexPhone))
				{
					phoneNumberError.Text = "Ecrire un numéro valide";
					isValid = false;
					error++;
				}
				else
				{
					customers.phoneNumber = phoneNumber.Text;
					phoneNumberError.Text = "";
				}
			}
			else
			{
				phoneNumberError.Text = "Ecrire un numéro";
				isValid = false;
				error++;
			}
			//BUDGET
			if (!String.IsNullOrEmpty(budget.Text))
			{
				if(!Regex.IsMatch(budget.Text, regexBudget))
				{
					budgetError.Text = "Donner la valeur du budget";
					isValid = false;
					error++;
				}
				else
				{
					customers.budget = int.Parse(budget.Text);
					budgetError.Text = "";
				}
			}
			else
			{
				budgetError.Text = "Ecrire un budget";
				isValid = false;
				error++;
			}
			//SUBJECT
			if (!String.IsNullOrEmpty(subject.Text))
			{
				if(!Regex.IsMatch(subject.Text, regexSubject))
				{
					subjectError.Text = "Ecrire un sujet valide";
					isValid = false;
					error++;
				}
				else
				{
					customers.subject = subject.Text;
					subjectError.Text = "";
				}
			}
			else
			{
				subjectError.Text = "Ecrire un sujet";
				isValid = false;
				error++;
			}
			//SAUVEGARDE ET RESET
			if (isValid == true)
			{
				db.customers.Add(customers);
				db.SaveChanges();
				lastName.Text = "";
				firstName.Text = "";
				mail.Text = "";
				phoneNumber.Text = "";
				budget.Text = "";
				subject.Text = "";
				MessageBox.Show("Votre Client à bien été ajouté", "Succès");
			}
			else
			{
				MessageBox.Show("Vous avez " + error + " Erreur(s)");
			}
		}

		private void Deneid_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("êtes-vous sûr de vouloir Annuler ?", "Confirmation",System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				lastName.Text = "";
				firstName.Text = "";
				mail.Text = "";
				phoneNumber.Text = "";
				budget.Text = "";
				subject.Text = "";
				lastNameError.Text = "";
				firstNameError.Text = "";
				mailError.Text = "";
				phoneNumberError.Text = "";
				budgetError.Text = "";
				subjectError.Text = "";
			}
		}
	}
}