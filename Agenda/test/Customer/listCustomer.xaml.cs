﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace test.Customer
{
	/// <summary>
	/// Logique d'interaction pour listCustomer.xaml
	/// </summary>
	public partial class listCustomer : Page
	{
		private agenda db = new agenda();
		customers customer;
		int? idtodel;
		string regexName = @"^[A-Za-zéèàêâôûùïüç\-]+$";
		string regexSubject = @"^[A-Z0-9a-zéèàêâôûùïüç '\-]+$";
		string regexMail = @"[0-9a-zA-Z\.\-]+@[0-9a-zA-Z\.\-]+.[a-zA-Z]{2,4}";
		string regexPhone = @"^[0][0-9]{9}";
		string regexBudget = @"^[0-9]";

		public listCustomer()
		{
			InitializeComponent();
			// On instancie ici pour eviter des reset impromptu.
			customer = new customers();
		}

		// Au chargement, charge une tableau avec db.customers comme source.
		private void Grid_Loaded(object sender, RoutedEventArgs e)
		{
			customersDataGrid.ItemsSource = db.customers.ToList();
		}

		// Remplissage des champ EditXXXXX par les colonnes selectionné.
		private void CustomersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (customersDataGrid.SelectedItem == null) return; // A mis cuission retourner la variable
			customer = customersDataGrid.SelectedItem as customers;
			idtodel = customer.idCustomer;

			editLastName.Text = customer.lastname;
			editFirstName.Text = customer.firstname;
			editMail.Text = customer.mail;
			editPhoneNumber.Text = customer.phoneNumber;
			editBudget.Text = customer.budget.ToString();
			editSubject.Text = customer.subject;
		}

		// Bouton "Annuler"
		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
			// Confirmation 
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir annuler ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				// Vidage des champs
				editLastName.Text = "";
				editFirstName.Text = "";
				editMail.Text = "";
				editPhoneNumber.Text = "";
				editBudget.Text = "";
				editSubject.Text = "";
				lastNameError.Text = "";
				firstNameError.Text = "";
				mailError.Text = "";
				phoneNumberError.Text = "";
				budgetError.Text = "";
				subjectError.Text = "";
			}
		}

		// Bouton de modification
		private void ButtonModify_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir modifier ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				//instanciation
				customers customers = new customers();
				bool isValid = true;
				int error = 0;

				//Insertion
				//LASTNAME
				if (!String.IsNullOrEmpty(editLastName.Text))
				{
					if (!Regex.IsMatch(editLastName.Text, regexName))
					{
						lastNameError.Text = "Ecrire un nom valide";
						isValid = false;
						error++;
					}
					else
					{
						customers.lastname = editLastName.Text;
						lastNameError.Text = "";
					}
				}
				else
				{
					lastNameError.Text = "Ecrire un nom";
					isValid = false;
					error++;
				}
				//FIRSTNAME
				if (!String.IsNullOrEmpty(editFirstName.Text))
				{
					if (!Regex.IsMatch(editFirstName.Text, regexName))
					{
						firstNameError.Text = "Ecrire un prenom valide";
						isValid = false;
						error++;
					}
					else
					{
						customers.firstname = editFirstName.Text;
						firstNameError.Text = "";
					}
				}
				else
				{
					firstNameError.Text = "Ecrire un prenom";
					isValid = false;
					error++;
				}
				//MAIL
				if (!String.IsNullOrEmpty(editMail.Text))
				{
					if (!Regex.IsMatch(editMail.Text, regexMail))
					{
						mailError.Text = "Ecrire un mail valide";
						isValid = false;
						error++;
					}
					else
					{
						customers.mail = editMail.Text;
						mailError.Text = "";
					}
				}
				else
				{
					mailError.Text = "Ecrire un Mail";
					isValid = false;
					error++;
				}
				//PHONENUMBER
				if (!String.IsNullOrEmpty(editPhoneNumber.Text))
				{
					if (!Regex.IsMatch(editPhoneNumber.Text, regexPhone))
					{
						phoneNumberError.Text = "Ecrire un numéro valide";
						isValid = false;
						error++;
					}
					else
					{
						customers.phoneNumber = editPhoneNumber.Text;
						phoneNumberError.Text = "";
					}
				}
				else
				{
					phoneNumberError.Text = "Ecrire un numéro";
					isValid = false;
					error++;
				}
				//BUDGET
				if (!String.IsNullOrEmpty(editBudget.Text))
				{
					if (!Regex.IsMatch(editBudget.Text, regexBudget))
					{
						budgetError.Text = "Donner la valeur du budget";
						isValid = false;
						error++;
					}
					else
					{
						customers.budget = int.Parse(editBudget.Text);
						budgetError.Text = "";
					}
				}
				else
				{
					budgetError.Text = "Ecrire un budget";
					isValid = false;
					error++;
				}
				//SUBJECT
				if (!String.IsNullOrEmpty(editSubject.Text))
				{
					if (!Regex.IsMatch(editSubject.Text, regexSubject))
					{
						subjectError.Text = "Ecrire un sujet valide";
						isValid = false;
						error++;
					}
					else
					{
						customers.subject = customer.subject = editSubject.Text;
						subjectError.Text = "";
					}
				}
				else
				{
					subjectError.Text = "Ecrire un sujet";
					isValid = false;
					error++;
				}
				if (isValid == true)
				{
					//MessageBox.Show(customer.lastName +"\n"+ customer.firstName +"\n" + customer.mail + "\n" + customer.phoneNumber + "\n" + customer.budget + "\n" + customer.subject);
					db.Entry(customer).State = EntityState.Modified;
					db.SaveChanges();
					customersDataGrid.Items.Refresh();

					// Reset des textbox
					editLastName.Text = "";
					editFirstName.Text = "";
					editMail.Text = "";
					editPhoneNumber.Text = "";
					editBudget.Text = "";
					editSubject.Text = "";

					MessageBox.Show("Le client a bien été modifié", "Succes");
				}
				else
				{
					MessageBox.Show("Vous avez " + error + " Erreur(s)");
				}
			}
		}

		private void ButtonDelete_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir supprimer ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				if (idtodel != null)
				{
					customers customers = db.customers.Find(idtodel);
					db.customers.Remove(customer);
					db.SaveChanges();
					customersDataGrid.ItemsSource = null;
					customersDataGrid.ItemsSource = db.customers.ToList();
				}
				else
				{
					messageBoxResult = System.Windows.MessageBox.Show("Choisisez un Client a supprimer", "Erreur");
				}
			}
		}
	}
}