namespace test
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class agenda : DbContext
	{
		public agenda()
			: base("name=agenda")
		{
		}

		public virtual DbSet<appointments> appointments { get; set; }
		public virtual DbSet<brokers> brokers { get; set; }
		public virtual DbSet<customers> customers { get; set; }
		public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<brokers>()
				.Property(e => e.lastname)
				.IsUnicode(false);

			modelBuilder.Entity<brokers>()
				.Property(e => e.firstname)
				.IsUnicode(false);

			modelBuilder.Entity<brokers>()
				.Property(e => e.mail)
				.IsUnicode(false);

			modelBuilder.Entity<brokers>()
				.Property(e => e.phoneNumber)
				.IsUnicode(false);

			modelBuilder.Entity<brokers>()
				.HasMany(e => e.appointments)
				.WithRequired(e => e.brokers)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<customers>()
				.Property(e => e.lastname)
				.IsUnicode(false);

			modelBuilder.Entity<customers>()
				.Property(e => e.firstname)
				.IsUnicode(false);

			modelBuilder.Entity<customers>()
				.Property(e => e.mail)
				.IsUnicode(false);

			modelBuilder.Entity<customers>()
				.Property(e => e.phoneNumber)
				.IsUnicode(false);

			modelBuilder.Entity<customers>()
				.Property(e => e.subject)
				.IsUnicode(false);
		}
	}
}