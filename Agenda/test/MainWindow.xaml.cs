﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace test
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void addCustomer_Click(object sender, RoutedEventArgs e)
		{
			display.Navigate(new Customer.addCustomer());
		}

		private void listCustomer_Click(object sender, RoutedEventArgs e)
		{
			display.Navigate(new Customer.listCustomer());
		}

		private void listBroker_Click(object sender, RoutedEventArgs e)
		{
			display.Navigate(new Broker.listBroker());
		}

		private void addBroker_Click(object sender, RoutedEventArgs e)
		{
			display.Navigate(new Broker.addBroker());
		}
	}
}