﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace test.Broker
{
	/// <summary>
	/// Logique d'interaction pour addBroker.xaml
	/// </summary>
	public partial class addBroker : Page
	{
		private agenda db = new agenda();
		string regexName = @"^[A-Za-zéèàêâôûùïüç\-]+$";
		string regexMail = @"[0-9a-zA-Z\.\-]+@[0-9a-zA-Z\.\-]+.[a-zA-Z]{2,4}";
		string regexPhone = @"^[0][0-9]{9}";

		public addBroker()
		{
			InitializeComponent();
		}
		private void Seend_Click(object sender, RoutedEventArgs e)
		{
			//instanciation
			brokers brokers = new brokers();
			bool isValid = true;
			int error = 0;

			//Insertion
			//LASTNAME
			if (!String.IsNullOrEmpty(lastName.Text))
			{
				if (!Regex.IsMatch(lastName.Text, regexName))
				{
					lastNameError.Text = "Ecrire un nom valide";
					isValid = false;
					error++;
				}
				else
				{
					brokers.lastname = lastName.Text;
					lastNameError.Text = "";
				}
			}
			else
			{
				lastNameError.Text = "Ecrire un nom";
				isValid = false;
				error++;
			}
			//FIRSTNAME
			if (!String.IsNullOrEmpty(firstName.Text))
			{
				if (!Regex.IsMatch(firstName.Text, regexName))
				{
					firstNameError.Text = "Ecrire un prenom valide";
					isValid = false;
					error++;
				}
				else
				{
					brokers.firstname = firstName.Text;
					firstNameError.Text = "";
				}
			}
			else
			{
				firstNameError.Text = "Ecrire un prenom";
				isValid = false;
				error++;
			}
			//MAIL
			if (!String.IsNullOrEmpty(mail.Text))
			{
				if (!Regex.IsMatch(mail.Text, regexMail))
				{
					mailError.Text = "Ecrire un mail valide";
					isValid = false;
					error++;
				}
				else
				{
					brokers.mail = mail.Text;
					mailError.Text = "";
				}
			}
			else
			{
				mailError.Text = "Ecrire un Mail";
				isValid = false;
				error++;
			}
			//PHONENUMBER
			if (!String.IsNullOrEmpty(phoneNumber.Text))
			{
				if (!Regex.IsMatch(phoneNumber.Text, regexPhone))
				{
					phoneNumberError.Text = "Ecrire un numéro valide";
					isValid = false;
					error++;
				}
				else
				{
					brokers.phoneNumber = phoneNumber.Text;
					phoneNumberError.Text = "";
				}
			}
			else
			{
				phoneNumberError.Text = "Ecrire un numéro";
				isValid = false;
				error++;
			}
			//SAUVEGARDE ET RESET
			if (isValid == true)
			{
				db.brokers.Add(brokers);
				db.SaveChanges();
				lastName.Text = "";
				firstName.Text = "";
				mail.Text = "";
				phoneNumber.Text = "";

				MessageBox.Show("Votre Client à bien été ajouté", "Sucès");
			}
			else
			{
				MessageBox.Show("Vous avez " + error + " Erreur(s)");
			}
		}
		private void Deneid_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("êtes-vous sûr de vouloir Annuler ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				lastName.Text = "";
				firstName.Text = "";
				mail.Text = "";
				phoneNumber.Text = "";
				lastNameError.Text = "";
				firstNameError.Text = "";
				mailError.Text = "";
				phoneNumberError.Text = "";
			}
		}
	}
}