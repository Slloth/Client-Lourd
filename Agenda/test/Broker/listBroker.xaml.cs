﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace test.Broker
{
	public partial class listBroker : Page
	{
		// Instanciation de la db
		private agenda db = new agenda();
		// Intanciation de broker ici et dans la methode en dessous pour eviter de perdre des données.
		brokers broker;
		// Variable pour refresh post-suppression
		int? idToDel;

		public listBroker()
		{
			InitializeComponent();
			broker = new brokers();
		}

		/// <summary>
		/// Methode de chargement de la liste
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Grid_Loaded(object sender, RoutedEventArgs e)
		{
			brokersDataGrid.ItemsSource = db.brokers.ToList();
		}

		/// <summary>
		/// Methode de selection pour edit.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void BrokersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			// Necessaire au refresh post-suppression
			if (brokersDataGrid.SelectedItem == null) return; // A mi cuisson, 5 mn de chaque coté, pensez a saler et poivrer.
			broker = brokersDataGrid.SelectedItem as brokers;
			idToDel = broker.idBroker;

			// On attribue a chaque champ prévu la valeur de l'element selectionner
			editLastName.Text = broker.lastname;
			editFirstName.Text = broker.firstname;
			editMail.Text = broker.mail;
			editPhoneNumber.Text = broker.phoneNumber;
		}

		/// <summary>
		/// Methode d'annulation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
			// Confirmation 
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir annuler ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				// On vide des champs
				editLastName.Text = "";
				editFirstName.Text = "";
				editMail.Text = "";
				editPhoneNumber.Text = "";

				lastNameError.Text = "";
				firstNameError.Text = "";
				mailError.Text = "";
				phoneNumberError.Text = "";

			}
		}

		/// <summary>
		/// Methode d'edition
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ButtonModify_Click(object sender, RoutedEventArgs e)
		{
			// Confirmation
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir modifier ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				// Si oui on lance le processus d'edition :

				// Création des variables nécéssaire au regex 
				string regexName = @"^[A-Za-zéèàêâôûùïüç\-]+$";
				string regexMail = @"[0-9a-zA-Z\.\-]+@[0-9a-zA-Z\.\-]+.[a-zA-Z]{2,4}";
				string regexPhone = @"^[0][0-9]{9}";
				bool isValid = true;
				// Compteur d'erreur remit a 0 a chaque opération
				int error = 0;

				/// Verification de la saisie de LASTNAME
				if (!String.IsNullOrEmpty(editLastName.Text))
				{
					if (!Regex.IsMatch(editLastName.Text, regexName))
					{
						lastNameError.Text = "Nom non valide";
						isValid = false;
						// On compte les erreurs en cas de champ invalide et/ou vide
						error++;
					}
					else
					{
						// QUand tout est validé via la regex on enregistre l'input et on vide le champ
						broker.lastname = editLastName.Text;
						lastNameError.Text = "";
					}
				}
				else
				{
					lastNameError.Text = "Veuillez saisir un nom";
					isValid = false;
					error++;
				}

				// Verificatoin pour FIRSTNAME
				if (!String.IsNullOrEmpty(editFirstName.Text))
				{
					if (!Regex.IsMatch(editFirstName.Text, regexName))
					{
						firstNameError.Text = "Prénom non valide";
						isValid = false;
						// Incrementation du compteur d'erreur si il y en a une 
						error++;
					}
					else
					{
						// Enregistrement et reset du champ aprés validation
						broker.firstname = editFirstName.Text;
						firstNameError.Text = "";
					}
				}
				else
				{
					firstNameError.Text = "Veuillez saisir un prénom";
					isValid = false;
					// Compteur d'erreur
					error++;
				}

				// Verifification pour le MAIL : 
				if (!String.IsNullOrEmpty(editMail.Text))
				{
					if (!Regex.IsMatch(editMail.Text, regexMail))
					{
						mailError.Text = "Mail non valide";
						isValid = false;
						error++;
					}
					else
					{
						broker.mail = editMail.Text;
						mailError.Text = "";
					}
				}
				else
				{
					mailError.Text = "Ecrire un Mail";
					isValid = false;
					error++;
				}

				// Verification PHONENUMBER
				if (!String.IsNullOrEmpty(editPhoneNumber.Text))
				{
					if (!Regex.IsMatch(editPhoneNumber.Text, regexPhone))
					{
						phoneNumberError.Text = "Ecrire un numéro valide";
						isValid = false;
						error++;
					}
					else
					{
						broker.phoneNumber = editPhoneNumber.Text;
						phoneNumberError.Text = "";
					}
				}
				else
				{
					phoneNumberError.Text = "Ecrire un numéro";
					isValid = false;
					error++;
				}


				// Enregistrement des modifs
				db.Entry(broker).State = EntityState.Modified;
				db.SaveChanges();
				// Refresh
				brokersDataGrid.Items.Refresh();

				// Reset des textbox
				editLastName.Text = "";
				editFirstName.Text = "";
				editMail.Text = "";
				editPhoneNumber.Text = "";

				MessageBox.Show("Le client a bien été modifié", "Succes");

			}
			else
			{

			}
		}

		/// <summary>
		///  Methode de suppression
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ButtonDelete_Click(object sender, RoutedEventArgs e)
		{
			// Message de confirmation
			MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Êtes vous sur de vouloir supprimer ce courtier ?", "Confirmation", System.Windows.MessageBoxButton.YesNo);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				// Instanciation via l'ID stocké
				brokers broker = db.brokers.Find(idToDel);

				// supression
				db.brokers.Remove(broker);
				// Sauvegarde 
				db.SaveChanges();

				// Refresh + alert de confirmation.
				brokersDataGrid.ItemsSource = null;
				brokersDataGrid.ItemsSource = db.brokers.ToList();
				MessageBox.Show("Le client a été supprimé", "Succes");
			}
		}
	}
}
