﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TP_2
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		long number1 = 0;
		long number2 = 0;
		string operation = "";

		public MainWindow()
		{
			InitializeComponent();
		}

		//DETECTION DU NOMBRE

		private void Button0_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 0;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 0;
				result.Text = number2.ToString();
			}
		}

		private void Button1_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 1;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 1;
				result.Text = number2.ToString();
			}
		}

		private void Button2_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 2;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 2;
				result.Text = number2.ToString();
			}
		}

		private void Button3_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 3;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 3;
				result.Text = number2.ToString();
			}
		}

		private void Button4_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 4;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 4;
				result.Text = number2.ToString();
			}
		}

		private void Button5_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 5;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 5;
				result.Text = number2.ToString();
			}
		}

		private void Button6_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 6;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 6;
				result.Text = number2.ToString();
			}
		}

		private void Button7_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 7;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 7;
				result.Text = number2.ToString();
			}
		}

		private void Button8_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 8;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 8;
				result.Text = number2.ToString();
			}
		}

		private void Button9_click(object sender, RoutedEventArgs e)
		{
			if (operation == "")
			{
				number1 = (number1 * 10) + 9;
				result.Text = number1.ToString();
			}
			else
			{
				number2 = (number2 * 10) + 9;
				result.Text = number2.ToString();
			}
		}

		//CHOIX DE L'OPERATEUR

		private void ButtonDivide_click(object sender, RoutedEventArgs e)
		{
			operation = "/";
		}

		private void ButtonTime_click(object sender, RoutedEventArgs e)
		{
			operation = "*";
		}

		private void ButtonLess_click(object sender, RoutedEventArgs e)
		{
			operation = "-";
		}

		private void ButtonMore_click(object sender, RoutedEventArgs e)
		{
			operation = "+";
		}

		//CALCULE

		private void Button_equal(object sender, RoutedEventArgs e)
		{
			switch (operation)
			{
				case "+":
					result.Text = (number1 + number2).ToString();
					number1 = number1 + number2;
					number2 = 0;
					break;
				case "-":
					result.Text = (number1 - number2).ToString();
					number1 = number1 - number2;
					number2 = 0;
					break;
				case "/":
					result.Text = (number1 / number2).ToString();
					number1 = number1 / number2;
					number2 = 0;
					break;
				case "*":
					result.Text = (number1 * number2).ToString();
					number1 = number1 * number2;
					number2 = 0;
					break;
			}
		}

		//RESET

		private void ButtonReset_click(object sender, RoutedEventArgs e)
		{
			number1 = 0;
			number2 = 0;
			operation = "";
			result.Text = "0";
		}
	}
}